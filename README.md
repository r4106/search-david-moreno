# Rocker coding exercise

## Overview

This project contains a solution to the coding exercise described
on [[https://gist.github.com/rockerrecruit/236f7f53f055253e0b71695af7c81ed8]]

## Instructions

The project has been developed in Scala 2.13, and is managed with `sbt` (version 1.6.2).

To run the tests, use:

```
sbt test
```

To run the programme, use:

```
sbt runMain rocker.SearchMain directory-with-files-to-search
```

After indexing the existing files, a prompt will be shown: `"search (INTRO to finish)>"`.

Then you can provide a list of words, separated by commas.
Those words will be looked for in those files and a list of file names containing those words will be shown,
allong with a ranking indicating the percent of the given words that are present on each file, regardles of case.
Files not containing any word will not be shown.

## Design decisions

* A word is a sequence of characters delimited by word boundaries, as defined on regular expressions.
* Two words are equal iif they contain the same characters in the same order, irrespective of their case ("WORD" is equal to "Word" and "word").
* Tests are based on `Specs2` framework ([[https://etorreborre.github.io/specs2/]]).
* The ranking is the percent of the distinct provided words that are present on each file.
* Files without any match are not shown (can be changed if necessary).
* The data structure that has been chosen to represent the index is a trade-off between performance and memory use. It's optimised for a scenario of potentially many files in the directory, with many words present in multiple files.
* To finish the program, you can press Intro at the prompt.
* Some classes have been refactored to be easier to test. There is still some work to do in this area.
* Some of the code provided on the assignment description has been used, but not all. The provided code is trusted and not covered by tests.
* A file containing unmappeable characters is skipped. That gets rid of binary files, but it also means that some text files with exotic characters are ignored.
