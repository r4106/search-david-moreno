ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .settings(
    name := "search-david-moreno"
  )

libraryDependencies ++= Seq("org.specs2" %% "specs2-core" % "4.15.0" % "test")
libraryDependencies ++= Seq("org.specs2" %% "specs2-mock" % "4.15.0" % "test")
