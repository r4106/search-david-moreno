package rocker

import rocker.io.{FileReading, UserInteraction}

/**
 * Main object of the program, which handles the execution.
 */
object SearchMain {

  /**
   * Reads a directory name and runs the search loop until the user presses Intro.
   *
   * @param args an array of Strings with a directory name as its first element
   */
  def main(args: Array[String]): Unit = {
    val fileRating = new FileRating(new FileReading)

    val index = fileRating.index(args)

    if (index.nonEmpty) {

      val fileRatingsWithIndex = fileRating.fileRatings(index) _

      Iterator.continually(UserInteraction.readSearchStringViaPrompt())
        .takeWhile(_.nonEmpty)
        .foreach(searchString => UserInteraction.showFileRankings(fileRatingsWithIndex(searchString)))

    } else {
      println("Could not find any text files to search")
    }
  }
}
