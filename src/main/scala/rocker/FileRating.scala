package rocker

import rocker.FileRating._
import rocker.io.FileReading

import java.util.regex.Pattern


/**
 * Object with functions to perform the word search on files and compute
 * their matching rating.
 */
class FileRating(private val fileReading: FileReading) {

  /**
   * Returns an index to speed up the search of words on files hosted in
   * the directory specified in args.
   *
   * @param args provided command line arguments, which should contain a
   *             directory as its first element
   * @return an index to speed up the search of words on files
   */
  def index(args: Array[String]): Index = {

    val wordFileColl = for {

      file <- fileReading.filesInDirectory(args)
      line <- fileReading.linesInFile(file)
      word <- WordSplittingPattern.split(line)

    } yield (word.toUpperCase, file.getName)

    def distinctFiles(wordFileTuples: Array[(String, String)]) = wordFileTuples.map(_._2).distinct

    wordFileColl.groupBy(_._1).view.mapValues(distinctFiles).toMap

  }

  /**
   * Return the [[MaxFilesToShow]] files with the highest ratings, where those ratings indicate
   * which percent of the unique words in searchString are contained on them.
   *
   * @param indexedFiles index to perform the word searches
   * @param searchString words to search for
   * @return the [[MaxFilesToShow]] files with the highest ratings
   */
  def fileRatings(indexedFiles: Index)(searchString: String): Seq[(String, Int)] = {

    val distinctUppercaseInputWords = WordSplittingPattern.split(searchString).filterNot(_.isEmpty).map(_.toUpperCase).toSet

    val inputWordsCount = distinctUppercaseInputWords.size

    if (inputWordsCount != 0) {

      val fileMatches = indexedFiles.view.filterKeys(distinctUppercaseInputWords).values.flatten

      val fileAndMatchCountMap = fileMatches
        .groupBy(identity)
        .view.mapValues(_.size)

      fileAndMatchCountMap
        .toSeq.sortBy(-_._2)
        .take(MaxFilesToShow) /* TECH-DEBT: An ad-hoc algorithm to take the top n elements (based, for example,
                              on selection sort algorithm) would perform better */
        .map {
          case (fileName, occurrenceCount) => (fileName, (occurrenceCount * 100) / inputWordsCount)
        }

    } else {
      Seq.empty[(String, Int)]
    }
  }
}

object FileRating {

  private type Index = Map[String, Array[String]]
  val MaxFilesToShow = 10
  private val WordSplittingPattern = Pattern.compile("\\W+")

}
