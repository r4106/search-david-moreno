package rocker.io

import scala.io.StdIn.readLine

/**
 * Object that contains functions for user interaction
 */
object UserInteraction {

  /**
   * Show a prompt on standard output and return the text
   * provided by the user.
   *
   * @return the text provided by the user.
   */
  def readSearchStringViaPrompt(): String = {

    print(s"search (INTRO to finish)> ")

    readLine()
  }

  /**
   * Show a sequence of file names and rankings on the
   * standard output.
   *
   * @param fileRankings a sequence of file names and rankings
   */
  def showFileRankings(fileRankings: Seq[(String, Int)]): Unit =
    fileRankings.foreach {
      case (fileName, rating) => println(s"$fileName: $rating%")
    }
}
