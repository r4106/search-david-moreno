package rocker.io

import java.io.File
import scala.io.Source
import scala.util.{Try, Using}

/**
 * Object with functions for file reading.
 */
class FileReading {

  def filesInDirectory(directoryNames: Array[String]): Array[File] = readDirectory(directoryNames).fold(
    { error => println(error); Array.empty[File] },
    dir => dir.listFiles(_.isFile)
  )

  def readDirectory(args: Array[String]): Either[ReadDirectoryError, File] =
    for {
      path <- args.headOption.toRight(MissingPathArg)
      directory <- Try(new java.io.File(path)).fold(
        throwable => Left(DirectoryNotFound(throwable)),
        file =>
          if (file.isDirectory) Right(file)
          else Left(NotDirectory(s"Path [$path] is not a directory"))
      )
    } yield directory

  def linesInFile(file: File): Array[String] =
    Using.resource(Source.fromFile(file))(source =>
      Try(source.getLines()).fold(ex => {
        println(s"File $file cannot be read as a text file. Skipping it.")
        ex.printStackTrace()
        Array.empty[String]
      },
        _.toArray)
    ) // Traded efficiency for legibility. toArray eagerly consumes the getLines iterator, but allows us to use a simple for expression

  sealed trait ReadDirectoryError

  case class NotDirectory(error: String) extends ReadDirectoryError

  case class DirectoryNotFound(t: Throwable) extends ReadDirectoryError

  case object MissingPathArg extends ReadDirectoryError
}
