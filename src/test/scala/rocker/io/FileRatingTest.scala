package rocker.io

import org.specs2.mock.Mockito
import org.specs2.mutable.Specification
import rocker.FileRating

import java.io.File

class FileRatingTest extends Specification with Mockito {

  "index" should {
    "return an index with every word linked to the files where it's found, when applied to a directory with files" in {

      // Set up
      val testDirName: Array[String] = Array("test_dir")
      val mockFileReading = Mockito.mock[FileReading]
      val mockFile1 = Mockito.mock[File]
      val mockFile2 = Mockito.mock[File]
      val mockFile3 = Mockito.mock[File]

      mockFile1.getName returns "mockFile1"
      mockFile2.getName returns "mockFile2"
      mockFile3.getName returns "mockFile3"

      mockFileReading.filesInDirectory(testDirName) returns Array(mockFile1, mockFile2)

      mockFileReading.linesInFile(mockFile1) returns Array("The heart-ache, and the thousand natural shocks", "That Flesh is heir to? 'Tis a consummation")
      mockFileReading.linesInFile(mockFile2) returns Array("To be, or not to be, that is the question:")
      mockFileReading.linesInFile(mockFile3) returns Array.empty[String]

      val sutFileRating = new FileRating(mockFileReading)

      // Exercise
      val index = sutFileRating.index(testDirName)

      // Verify (array values converted to sets to avoid mismatches due to elements in different order)
      index.view.mapValues(_.toSet).toMap must havePairs("THE" -> Set("mockFile1", "mockFile2"), "HEART" -> Set("mockFile1"), "ACHE" -> Set("mockFile1"), "AND" -> Set("mockFile1"), "THOUSAND" -> Set("mockFile1"), "NATURAL" -> Set("mockFile1"), "SHOCKS" -> Set("mockFile1"), "THAT" -> Set("mockFile1", "mockFile2"), "FLESH" -> Set("mockFile1"), "IS" -> Set("mockFile1", "mockFile2"), "HEIR" -> Set("mockFile1"), "TO" -> Set("mockFile1", "mockFile2"), "TIS" -> Set("mockFile1"), "A" -> Set("mockFile1"), "CONSUMMATION" -> Set("mockFile1"), "BE" -> Set("mockFile2"), "OR" -> Set("mockFile2"), "NOT" -> Set("mockFile2"), "QUESTION" -> Set("mockFile2"))
    }
  }
  "return an empty index when applied to an empty directory" in {

    // Set up
    val testDirName: Array[String] = Array("test_dir")
    val mockFileReading = Mockito.mock[FileReading]

    mockFileReading.filesInDirectory(testDirName) returns Array.empty[File]

    val sutFileRating = new FileRating(mockFileReading)

    // Exercise
    val index = sutFileRating.index(testDirName)

    // Verify
    index must beEmpty
  }
  "fileRatings" should {
    "return an empty rating collection when an empty index is received" in {
      // Set up
      val sutFileRating = new FileRating(new FileReading)

      // Exercise
      val ratings = sutFileRating.fileRatings(Map.empty[String, Array[String]])("dummy string")

      // Verify
      ratings must beEmpty
    }
    "return an empty rating collection when an empty search string is received" in {
      // Set up
      val sutFileRating = new FileRating(new FileReading)

      // Exercise
      val ratings = sutFileRating.fileRatings(Map("dummyWord" -> Array("dummyFile")))("")

      // Verify
      ratings must beEmpty
    }
    "return 100% rating on a file where all the search words are found, irrespective of the case" in {
      // Set up
      val sutFileRating = new FileRating(new FileReading)

      val testFile = "file1"

      // Exercise
      val ratings = sutFileRating.fileRatings(Map("WORD1" -> Array(testFile), "WORD2" -> Array(testFile)))("Word1 word2")

      // Verify
      ratings must havePair(testFile, 100)
    }
    "return only the max number of files with the highest ratings, when there are more files available" in {
      // Set up
      val sutFileRating = new FileRating(new FileReading)

      val testFileWithLowestRanking = "testFile11"
      val allTheFiles = Array("testFile1", "testFile2", "testFile3", "testFile4", "testFile5", "testFile6", "testFile7", "testFile8", "testFile9", "testFile10", testFileWithLowestRanking)

      // Exercise
      val ratings = sutFileRating.fileRatings(Map("WORD1" -> allTheFiles, "WORD2" -> allTheFiles, "WORD3" -> allTheFiles.take(allTheFiles.length - 1)))("word1 word2 word3")

      // Verify
      ratings must haveSize(FileRating.MaxFilesToShow)
      (ratings.map(_._2) must contain(testFileWithLowestRanking)).not
    }
  }
}
